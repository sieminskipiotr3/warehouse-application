package mainPackage;

import java.util.concurrent.ThreadLocalRandom;

public abstract class InfiniteThread extends Thread {

    Market market;

    protected void job(){}

    public InfiniteThread (Market market) {
        this.market = market;
    }

    @Override
    public void run() {
        while (true) {
            job();
            try {
                Thread.sleep(2000);
            } catch (InterruptedException exception) {
                exception.printStackTrace();
            }
        }
    }
}
