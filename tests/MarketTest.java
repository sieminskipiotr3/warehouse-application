package mainPackage;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class MarketTest {

    @Test
    public void addingItemsException() {

        var market = new Market();
        String item = "Shoes";
        Integer quantity = 0;

        var exception = assertThrows(IllegalArgumentException.class, () -> {
            market.addItems(item, quantity);
        });

        String expected = "Quantity must be greater than 0";
        String actual = exception.getMessage();

        assertTrue(actual.contains(expected));

    }

    @Test
    public void consumeItem() {

        var market = new Market();
        String item = "T-shirt";
        int currVal = market.getSaleItemsDict().get(item);

        market.consumeItem(item);

        int expected = currVal--;
        int actual = market.getSaleItemsDict().get(item);

        assertEquals(expected, actual);

    }

}