package mainPackage;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ProdThreadTest {

    @Test
    public void addItemsTest() {
        var market = new Market();
        String item = "Hoodie";
        var thread = new ProdThread(market, item);
        int currVal = market.getSaleItemsDict().get(item);

        thread.start();

        int expected = currVal + 5;
        int actual = market.getSaleItemsDict().get(item);

        assertEquals(expected, actual);

    }

}