package mainPackage;

public class ProdThread extends InfiniteThread {

    private String item;

    public ProdThread (Market market, String item) {
        super(market);
        this.item = item;
    }

    @Override
    protected void job(){
        market.addItems(item, 5);
    }
}
