package mainPackage;

public class ConsThread extends InfiniteThread {

    private String item;

    public ConsThread (Market market, String item) {
        super(market);
        this.item = item;
    }

    @Override
    protected void job() {
        market.consumeItem(item);
    }
}