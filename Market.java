package mainPackage;

import java.util.Hashtable;

// This is on-line shopping storing information about products and its number in stock

public class Market {

    private Object lockObj = new Object();
    private Hashtable<String, Integer> saleItemsDict;

    public Market() {
        saleItemsDict = new Hashtable<String, Integer>();
        saleItemsDict.put("Jeans", 80);
        saleItemsDict.put("Hoodie", 300);
        saleItemsDict.put("Shoes", 249);
        saleItemsDict.put("T-shirt",49);
        saleItemsDict.put("Underwear", 15);
    }

    public Hashtable<String, Integer> getSaleItemsDict () {
        return saleItemsDict;
    }

    public void addItems(String item, int quantity) {
        if (quantity <= 0) {throw new IllegalArgumentException("Quantity must be greater than 0");}
        Integer currVal;
        synchronized (lockObj) {
            currVal = saleItemsDict.get(item);
            saleItemsDict.put(item, currVal + quantity);
        }
        System.out.printf("You have added %s times %s \n", quantity, item);
    }

    public void consumeItem(String item) {
        Integer currVal;
        synchronized (lockObj) {
            currVal = saleItemsDict.get(item);
            if (currVal <= 0) {return;} // it should never go below 0; it's an additional precaution
            saleItemsDict.put(item,currVal--);
        }
        System.out.printf("You have deleted %s. Current stock: %s\n", item, saleItemsDict.get(item));
    }
}
