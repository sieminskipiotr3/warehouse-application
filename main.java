package mainPackage;

public class Main {

    public static void main(String[] args) {
        var market = new Market();

        //1 - Producer
        new ProdThread(market, "Jeans").start();
        //2 - Producer
        new ProdThread(market, "Hoodie").start();
        //3 - Producer
        new ProdThread(market, "Shoes").start();
        //4 - Producer
        new ProdThread(market,"T-shirt").start();
        //5 - Producer
        new ProdThread(market,"Underwear").start();

        //1 - Consumer
        new ConsThread(market,"Jeans").start();
        //2 - Consumer
        new ConsThread(market,"Jeans").start();
        //3 - Consumer
        new ConsThread(market,"Hoodie").start();
        //4 - Consumer
        new ConsThread(market,"Hoodie").start();
        //5 - Consumer
        new ConsThread(market,"Shoes").start();
        //6 - Consumer
        new ConsThread(market,"Shoes").start();
        //7 - Consumer
        new ConsThread(market,"T-shirt").start();
        //8 - Consumer
        new ConsThread(market,"T-shirt").start();
        //9 - Consumer
        new ConsThread(market,"Underwear").start();
        //10 - Consumer
        new ConsThread(market,"Underwear").start();

    }
}
