This application is constructed to serve in a warehouse/stock environment.

User can add (produce) or delete (consume) the products. 

There is 5 examples, which obviously can be extended to the scale needed. 

Threads are synchronized.

Unit tests in directory "tests".